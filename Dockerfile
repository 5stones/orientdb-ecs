FROM orientdb:2.2.37

RUN cp -a config .config-default \
  && rm -f bin/*.bat

COPY bin/docker-start bin/docker-start
COPY config/ .config-default/

CMD ["docker-start"]
