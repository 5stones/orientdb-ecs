variable "name" {
  description = "The name of the db (and target group)"
  default     = "orientdb"
}

variable "region" {
  default = "us-east-1"
}

variable "vpc_id" {
  description = "The id of the vpc to create the ecs cluster an ec2 instances in"
}

variable "subnets" {
  description = "The subnets to launch instances in (1 for each availability zone: a, b, and c)"
  default     = []
}

variable "alb" {
  description = "Name of the ALB which will have a new rule and target group"
}

variable "zone" {
  description = "The DNS zone to add an entry to"
}

variable "hostname" {
  description = "DNS hostname inside the zone"
}

variable "namespace_id" {
}

variable "instance_type" {
  default = "t3.small"
}

variable "volume_size" {
  default = "20"
}

variable "key_name" {
  description = "ssh key pair name"
}

variable "efs_id" {
  description = "Used for configs and backups (if specified)"
  default     = ""
}

variable "image" {
  default = "orientdb:2.2.37"
}

variable "distributed" {
  default = true
}
