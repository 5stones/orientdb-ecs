resource "aws_ecs_cluster" "main" {
  name = var.name
}

# find autoscaling groups for ecs cluster
resource "aws_autoscaling_group" "main" {
  name = "EC2ContainerService-${var.name}"

  vpc_zone_identifier = var.subnets
  launch_template {
    name    = aws_launch_template.main.name
    version = "$Latest"
  }

  #target_group_arns = []

  min_size         = local.instance_count
  desired_capacity = local.instance_count
  max_size         = local.instance_count

  health_check_type = "EC2"

  tags = [
    {
      key                 = "Name"
      value               = "ECS Instance - EC2ContainerService-${aws_ecs_cluster.main.name}"
      propagate_at_launch = true
    },
  ]

  enabled_metrics = ["GroupInServiceInstances"]

  protect_from_scale_in = true
}

locals {
  instance_count = var.distributed ? 3 : 1
}
