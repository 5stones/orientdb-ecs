locals {
  data_device = "/dev/xvdd"
  data_dir    = "/mnt/data"
}

resource "aws_launch_template" "main" {
  name = "EC2ContainerService-${var.name}"

  #description = "${var.description}"

  image_id      = data.aws_ami.ecs.id
  instance_type = var.instance_type

  vpc_security_group_ids = [aws_security_group.main.id]
  key_name               = var.key_name

  ebs_optimized = true
  iam_instance_profile {
    name = "ecsInstanceRole"
  }
  monitoring {
    enabled = true
  }

  user_data = base64encode(local.user_data)

  block_device_mappings {
    device_name = local.data_device
    ebs {
      volume_type           = "gp2"
      volume_size           = var.volume_size
      delete_on_termination = false
    }
  }

  tags = {
    Name = "ECS Instance - EC2ContainerService-${var.name}"
  }
}

# the most recent ECS image
data "aws_ami" "ecs" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-*.*.*-amazon-ecs-optimized"]
  }

  name_regex = "^amzn-ami-20\\d{2}\\.[01]\\d.[a-z]-amazon-ecs-optimized$"
}

locals {
  efs_block = <<EOF
# mount efs
- file_system_id_01=${var.efs_id}
- efs_directory=/mnt/efs
- mkdir -p $${efs_directory}
- echo "$${file_system_id_01}:/ $${efs_directory} efs tls,_netdev" >> /etc/fstab
- mount -a -t efs defaults
EOF

  user_data = <<EOF
#cloud-config
repo_update: true
repo_upgrade: all

packages:
- amazon-efs-utils

runcmd:
# ecs
- echo ECS_CLUSTER=${var.name} >> /etc/ecs/ecs.config
- echo ECS_BACKEND_HOST= >> /etc/ecs/ecs.config;
- echo ECS_RESERVED_MEMORY=160 >> /etc/ecs/ecs.config;

# ebs data volume
- mkfs -t ext4 ${local.data_device}
- mkdir -p ${local.data_dir}
- echo '${local.data_device} ${local.data_dir} ext4 defaults,nofail,noatime 0 2' >> /etc/fstab
- mount ${local.data_dir}

${var.efs_id == "" ? "" : local.efs_block}
EOF
}
