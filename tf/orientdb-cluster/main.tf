# define the iam target group along with alb rules and dns entries
module "target_group" {
  source = "git::https://gitlab.com/5stones/tf-modules//aws/lb/target-group?ref=v2.3.2"

  name     = var.name
  hostname = var.hostname
  alb      = var.alb
  zone     = var.zone
}

resource "aws_cloudwatch_log_group" "main" {
  name              = var.name
  retention_in_days = 3
}

# nodes in 3 availability zones

module "node_a" {
  source = "./node"

  az = "a"

  distributed  = var.distributed
  image        = var.image
  namespace_id = var.namespace_id
  region       = var.region
  service      = var.name

  ecs_cluster    = aws_ecs_cluster.main.name
  security_group = aws_security_group.main.id
  subnets        = aws_autoscaling_group.main.vpc_zone_identifier
  target_group   = module.target_group.arn
  task_role_arn  = aws_iam_role.main.arn
}

module "node_b" {
  source = "./node"

  az = "b"

  distributed  = var.distributed
  image        = var.image
  namespace_id = var.namespace_id
  region       = var.region
  service      = var.name

  ecs_cluster    = aws_ecs_cluster.main.name
  security_group = aws_security_group.main.id
  subnets        = aws_autoscaling_group.main.vpc_zone_identifier
  target_group   = module.target_group.arn
  task_role_arn  = aws_iam_role.main.arn
}

module "node_c" {
  source = "./node"

  az = "c"

  distributed  = var.distributed
  image        = var.image
  namespace_id = var.namespace_id
  region       = var.region
  service      = var.name

  ecs_cluster    = aws_ecs_cluster.main.name
  security_group = aws_security_group.main.id
  subnets        = aws_autoscaling_group.main.vpc_zone_identifier
  target_group   = module.target_group.arn
  task_role_arn  = aws_iam_role.main.arn
}
