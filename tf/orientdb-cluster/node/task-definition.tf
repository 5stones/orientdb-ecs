resource "aws_ecs_task_definition" "main" {
  family = "${var.service}-${var.az}"

  network_mode             = "host"
  requires_compatibilities = ["EC2"]
  task_role_arn            = var.task_role_arn

  volume {
    name      = "backup"
    host_path = "/mnt/efs/${var.service}/${var.az}/backup"
  }
  volume {
    name      = "config"
    host_path = "/mnt/efs/${var.service}/${var.az}/config"
  }
  volume {
    name      = "databases"
    host_path = "/mnt/data/databases"
  }

  container_definitions = <<EOF
[
  {
    "name": "orientdb",
    "image": "${var.image}",
    "cpu": 1024,
    "memoryReservation": 1536,
    "memory": 1536,
    "portMappings": [
      {
        "containerPort": 2480,
        "hostPort": 2480,
        "protocol": "tcp"
      },
      {
        "containerPort": 2424,
        "hostPort": 2424,
        "protocol": "tcp"
      },
      {
        "containerPort": 2434,
        "hostPort": 2434,
        "protocol": "tcp"
      }
    ],
    "essential": true,
    "mountPoints": [
      {
        "sourceVolume": "backup",
        "containerPath": "/orientdb/backup",
        "readOnly": false
      },
      {
        "sourceVolume": "config",
        "containerPath": "/orientdb/config",
        "readOnly": false
      },
      {
        "sourceVolume": "databases",
        "containerPath": "/orientdb/databases",
        "readOnly": false
      }
    ],
    "environment": [
      {
        "name": "AWS_DEFAULT_REGION",
        "value": "${var.region}"
      },
      {
        "name": "AWS_DISCOVERY_SECURITY_GROUP",
        "value": "${var.service}"
      },
      {
        "name": "ORIENTDB_NODE_NAME",
        "value": "${var.service}-${var.az}"
      },
      {
        "name": "ORIENTDB_OPTS_MEMORY",
        "value": "-Xms512m -Xmx512m"
      },
      {
        "name": "JAVA_OPTS_SCRIPT",
        "value": "-Djna.nosys=true -XX:+HeapDumpOnOutOfMemoryError -XX:MaxDirectMemorySize=1024M -Djava.awt.headless=true -Dfile.encoding=UTF8 -Drhino.opt.level=9"
      }
    ],
    "command": ${var.image == "orientdb:2.2.37" ? jsonencode(local.base_image_command) : "null"},
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${var.service}",
        "awslogs-region": "${var.region}",
        "awslogs-stream-prefix": "orientdb-${var.az}"
      }
    }
  }
]
EOF
}

locals {
  base_image_command = [
    "sh",
    "-c",
    "mkdir -p /tmp/orientdb && exec /orientdb/bin/server.sh -Ddistributed=${var.distributed ? "true" : "false"}",
  ]
}
