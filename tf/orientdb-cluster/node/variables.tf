variable "target_group" {
}

variable "namespace_id" {
}

variable "service" {
  default = "orientdb"
}

variable "az" {
  default = "a"
}

variable "region" {
  default = "us-east-1"
}

variable "ecs_cluster" {
  default = "default"
}

variable "security_group" {
}

variable "subnets" {
  default = []
}

variable "image" {
  default = "orientdb:2.2.37"
}

variable "task_role_arn" {
  description = "The task role for permissions of containers"
  default     = ""
}

variable "distributed" {
  default = true
}
