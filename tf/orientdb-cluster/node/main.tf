locals {
  name = "${var.service}-${var.az}"
}

# define the iam service (only run 1 at a time)
resource "aws_ecs_service" "main" {
  name            = local.name
  cluster         = var.ecs_cluster
  task_definition = aws_ecs_task_definition.main.arn
  launch_type     = "EC2"

  desired_count = 1

  deployment_minimum_healthy_percent = 0
  deployment_maximum_percent         = 100

  health_check_grace_period_seconds = 600

  #service_registries = {  # only 1 allowed
  #  registry_arn = "${aws_service_discovery_service.main.arn}"
  #  container_name = "orientdb"
  #  container_port = "2434"
  #  #port = 0
  #}

  load_balancer {
    target_group_arn = var.target_group
    container_name   = "orientdb"
    container_port   = "2480"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone == ${var.region}${var.az}"
  }

  #network_configuration {
  #  subnets = ["${var.subnets}"]
  #  security_groups = ["${var.security_group}"]
  #  assign_public_ip = false
  #}

  lifecycle {
    ignore_changes = [desired_count]
  }
}

# define the internal discovery services for clustering (no internal http SRV)
#resource "aws_service_discovery_service" "main" {
#  name = "${local.name}"
#
#  dns_config {
#    namespace_id = "${var.namespace_id}"
#
#    dns_records {
#      ttl  = 60
#      type = "A"
#    }
#  }
#
#  health_check_custom_config {
#    failure_threshold = 1
#  }
#}
