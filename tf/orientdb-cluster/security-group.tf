resource "aws_security_group" "main" {
  name        = var.name
  description = "Allow internal OrientDB traffic"
  vpc_id      = var.vpc_id

  # hazelcast for clustering
  ingress {
    from_port = 2434
    to_port   = 2434
    protocol  = "tcp"

    self = true
  }

  # binary
  ingress {
    from_port = 2424
    to_port   = 2424
    protocol  = "tcp"

    cidr_blocks      = [data.aws_vpc.main.cidr_block]
    ipv6_cidr_blocks = [data.aws_vpc.main.ipv6_cidr_block]

    self = true
  }

  # REST
  ingress {
    from_port = 2480
    to_port   = 2480
    protocol  = "tcp"

    cidr_blocks      = [data.aws_vpc.main.cidr_block]
    ipv6_cidr_blocks = [data.aws_vpc.main.ipv6_cidr_block]
  }

  # local ssh
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks      = [data.aws_vpc.main.cidr_block]
    ipv6_cidr_blocks = [data.aws_vpc.main.ipv6_cidr_block]
  }

  # allow outgoing traffic
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  #tags = {
  #  Name = ""
  #}
}

data "aws_vpc" "main" {
  id = var.vpc_id
}
