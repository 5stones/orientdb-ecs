# orientdb-ecs

## Export & Import

```
docker run --rm -it -v /mnt/efs/orientdb-staging/a/config/:/orientdb/config -v /mnt:/mnt orientdb:2.2.37 sh

# set memory options in console.sh to match -Xms3G -Xmx3g -XX:MaxDirectMemorySize=4014M
vi bin/console.sh

bin/console.sh
connect plocal:/mnt/efs/orientdb-production-a/databases/Scylla kmichalak
export database /mnt/efs/backups/scylla-a.export
exit

mv /mnt/efs/orientdb-production-a/databases/Scylla /mnt/efs/backup/corrupted-dbs/Scylla-a
bin/console.sh
create database plocal:/mnt/data/databases/Scylla
import database /mnt/efs/orientdb-staging/a/backup/scylla-a.export.gz -preserveClusterIDs=true
REPAIR DATABASE --fix-graph
REPAIR DATABASE --fix-links
exit
```
